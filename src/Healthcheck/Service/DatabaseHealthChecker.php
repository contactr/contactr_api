<?php

declare(strict_types=1);

namespace App\Healthcheck\Service;

use Doctrine\ORM\EntityManagerInterface;

class DatabaseHealthChecker
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function isHealth(): bool
    {
        $cnx = $this->entityManager->getConnection();
        if ($cnx->isConnected()) {
            return true;
        } else {
            return false;
        }
    }
}
