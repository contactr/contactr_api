<?php

namespace App\Healthcheck\Controller;

use App\Healthcheck\Service\DatabaseHealthChecker;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class HealthcheckController extends AbstractController
{
    public function healthcheckAppAction(): JsonResponse
    {
        return $this->json([
            'message' => 'Welcome to the jungle!',
        ]);
    }

    public function healthcheckDbAction(DatabaseHealthChecker $checker): JsonResponse
    {
        return $checker->isHealth() ? $this->json(['message' => 'ok']) : $this->json(['message' => 'brak konekszyn']);
    }
}
