<?php

namespace App\BusinessCard\Domain;

class Card
{
    private ?string $personalData = 'firstname_lastname';
    private int $socialCount = 0;

    public function __construct(?string $personalData = 'firstname_lastname')
    {
        $this->personalData = $personalData;
    }

    public function getPersonalData(): string
    {
        return $this->personalData;
    }

    public function setPersonalData(string $personalData): void
    {
        $this->personalData = $personalData;
    }

    public function getSocialCount(): int
    {
        return $this->socialCount;
    }

    public function setSocialCount(int $count): void
    {
        $this->socialCount = $count;
    }
}
