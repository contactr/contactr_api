<?php

namespace spec\App\BusinessCard\Domain;

use App\BusinessCard\Domain\Card;
use PhpSpec\Exception\Example\FailureException;
use PhpSpec\ObjectBehavior;

class CardSpec extends ObjectBehavior
{
    public function getMatchers(): array
    {
        return [
            'returnZero' => function($subject) {
                if (0 !== $subject) {
                    throw new FailureException(
                        sprintf('Returned value should be 0, is %s', $subject)
                    );
                }

                return true;
            },

        ];
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(Card::class);
    }

    public function it_should_default_return_firstname_and_lastname(): void
    {
        $this->getPersonalData()->shouldReturn('firstname_lastname');
    }

    public function it_should_allow_to_set_personal_data(): void
    {
        $this->setPersonalData('nowe_dane');

        $this->getPersonalData()->shouldReturn('nowe_dane');
    }

    public function it_should_return_zero_for_social_count(): void
    {
        $this->getSocialCount()->shouldReturnZero();
    }

    public function it_should_return_true_when_social_count_is_five(): void
    {
        $this->setSocialCount(6);
        $this->getSocialCount()->shouldBeGreaterThan(4);
    }

    public function it_should_be_constructed_with_params(): void
    {
        $this->beConstructedWith('admin_testerski');
        $this->getPersonalData()->shouldReturn('admin_testerski');
    }
}
